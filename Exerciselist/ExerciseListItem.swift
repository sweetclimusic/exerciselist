//
// Created by Ashlee on 2019-09-16.
// Copyright (c) 2019 Green Farm Games Ltd. All rights reserved.
//

import Foundation

class ExerciseListItem {
    var text = ""
    var checked = false

    public func toggleCheck() {
        self.checked = !self.checked
    }
}
