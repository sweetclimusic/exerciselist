//
//  ViewController.swift
//  Exerciselist
//
//  Created by Ashlee on 15/09/2019.
//  Copyright © 2019 Green Farm Games Ltd. All rights reserved.
//

import UIKit

class ExerciseListViewController: UITableViewController {
    var exerciseListItems = [ExerciseListItem]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let exercise1 = ExerciseListItem()
        exercise1.text = "50 Pushups"
        exerciseListItems.append(exercise1)

        let exercise2 = ExerciseListItem()
        exercise2.text = "20 Pull-ups"
        exerciseListItems.append(exercise2)

        let exercise3 = ExerciseListItem()
        exercise3.text = "50 Squats"
        exercise3.checked = true
        exerciseListItems.append(exercise3)

        let exercise4 = ExerciseListItem()
        exercise4.text = "50 Situps"
        exercise4.checked = true
        exerciseListItems.append(exercise4)

        let exercise5 = ExerciseListItem()
        exercise5.text = "50 Skippings Kicks"
        exercise5.checked = true
        exerciseListItems.append(exercise5)
    }
    // MARK:- Table View Data Source
    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int) -> Int {
        return exerciseListItems.count
    }
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: "ExerciseListItem",
            for: indexPath)
        // Add the following code
        let exercise = exerciseListItems[indexPath.row]
        // Add exercise to stored data
        ConfigureText(for: cell, with: exercise)
        ConfigureCheckMark(for: cell, with: exercise)
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Perform checkmark toggle if cell exists
        // by first finding the UITableViewCell object
        // tableView.cellForRow is for calling cells that are already displayed
        // not for dataSource and creating new cell like the tabelView(:cellforRowAt:) above.
        if let cell = tableView.cellForRow(at: indexPath){
            let exercise = exerciseListItems[indexPath.row]
            exercise.toggleCheck()
            ConfigureCheckMark(for: cell, with: exercise)
        }
        // Continue default handling.
        tableView.deselectRow(at: indexPath, animated: true)
        
    }

    private func ConfigureCheckMark(for cell: UITableViewCell, with exercise: ExerciseListItem) {
        if exercise.checked{
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
    }
    private func ConfigureText(for cell: UITableViewCell, with exercise: ExerciseListItem) {
        let label = cell.viewWithTag(1000) as! UILabel
        label.text = exercise.text
    }
}
